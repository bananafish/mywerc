<div id="main-img">
% if ( ! ~ $#xibit_prevpic 0) {
  <a href="%($xibit_prevpic%)#main-img">Previous</a>
% }
% if ( ! ~ $#xibit_nextpic 0 && ! ~ $#xibit_prevpic 0 ) {
/
% }
% if ( ! ~ $#xibit_nextpic 0) {
<a href="%($xibit_nextpic%)#main-img">Next</a>
% }
  <div class="main-img" id="mainpic">
    <a href="%($xibit_img%)">
      <img src="%($xibit_preview%)" />
    </a>
  </div>
% if ( ! ~ $#xibit_caption_file 0) {
  <div class="caption">
% md_handler $xibit_caption_file
  </div>
% }
</div>
