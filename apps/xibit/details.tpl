<div class="images-details">
<ul>
% for (pic in $xibit_pics) {
  <li>
    <a href="
%                    echo $pic |sed 's,(.*)\..+$,\1,g'
" class="thumb" >
      <img src="%( $xibit_thumbsdir^/^$pic %)" class="thumb"/>
    </a>
% identify $xibit_urlroot/^$pic |awk '{print $4}'
  </li>
% }
</ul>
</div>
